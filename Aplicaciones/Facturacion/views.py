from django.shortcuts import render, redirect
from .models import GeneroLiterario
from .models import Profesion
from .models import Autor
from .models import Libro
from django.utils.datastructures import MultiValueDictKeyError
from django.db import transaction

from django.contrib import messages
# Create your views here.

def listadoGeneros(request):
    generoBdd = GeneroLiterario.objects.all()
    return render(request, 'listadoGeneros.html', {'generos': generoBdd})

def guardarGenero(request):
    if request.method == 'POST':
        nombre_ac = request.POST.get("nombre_ac")
        fecha_creacion_ac = request.POST.get("fecha_creacion_ac")
        descripcion_ac = request.POST.get("descripcion_ac")
        fotografia_ac = request.FILES.get("fotografia_ac")

        # Insertando datos mediante el ORM de Django
        genero = GeneroLiterario.objects.create(
            nombre_ac=nombre_ac,
            descripcion_ac=descripcion_ac,
            fotografia_ac=fotografia_ac
        )

        messages.success(request, 'GENERO LITERARIO GUARDADO EXITOSAMENTE')
        return redirect('/')

def eliminarGenero(request, id_ac):
    try:
        generoEliminar = GeneroLiterario.objects.get(id_ac=id_ac)
        generoEliminar.delete()
        messages.success(request, 'GENERO LITERARIO ELIMINADO EXITOSAMENTE')
    except GeneroLiterario.DoesNotExist:
        messages.error(request, 'El género no existe')

    return redirect('/')

def editarGenero(request, id_ac):
    try:
        generoEditar = GeneroLiterario.objects.get(id_ac=id_ac)
        generosBdd = GeneroLiterario.objects.all()
        return render(request, 'editarGenero.html', {'genero': generoEditar, 'generos': generosBdd})
    except GeneroLiterario.DoesNotExist:
        messages.error(request, 'El género no existe')
        return redirect('/')

def procesarActualizacionGenero(request):
    if request.method == 'POST':
        id_ac = request.POST.get("id_ac")
        nombre_ac = request.POST.get("nombre_ac")
        descripcion_ac = request.POST.get("descripcion_ac")
        fecha_creacion_ac = request.POST.get("fecha_creacion_ac")
        fotografia_ac = request.FILES.get('fotografia_ac')

        try:
            generoEditar = GeneroLiterario.objects.get(id_ac=id_ac)

            if fotografia_ac is not None:
                generoEditar.fotografia_ac = fotografia_ac

            generoEditar.nombre_ac = nombre_ac
            generoEditar.fecha_creacion_ac = fecha_creacion_ac
            generoEditar.descripcion_bg = descripcion_bg
            generoEditar.save()

            messages.success(request, 'GENERO LITERARIO ACTUALIZADO EXITOSAMENTE')
        except GeneroLiterario.DoesNotExist:
            messages.error(request, 'El género no existe')

    return redirect('/')

def listadoProfesiones(request):
    profesionBdd = Profesion.objects.all()
    return render(request, 'listadoProfesiones.html', {'profesiones': profesionBdd})

def guardarProfesion(request):
    nombre_ac=request.POST["nombre_ac"]
    descripcion_ac=request.POST["descripcion_ac"]
    años_experiencia_ac=request.POST["años_experiencia_ac"]
    fotografia_ac=request.POST["fotografia_ac"]


    #Insertando datos mediante el ORM de django
    profesion =Profesion.objects.create(nombre_ac=nombre_ac,descripcion_ac=descripcion_ac,
    años_experiencia_ac=años_experiencia_ac)
    messages.success(request, 'PROFESION  GUARDADA EXITOSAMENTE')
    return redirect('/listadoProfesiones/')

def eliminarProfesion(request,id_ac):
    profesionEliminar=Profesion.objects.get(id_ac=id_ac)
    profesionEliminar.delete()
    messages.success(request, 'PROFESION ELIMINADA EXITOSAMENTE')
    return redirect('/listadoProfesiones/')

def editarProfesion(request, id_ac):
    profesionEditar=Profesion.objects.get(id_ac=id_ac)
    profesionesBdd=Profesion.objects.all()
    return  render(request, 'editarProfesion.html',{'profesion':profesionEditar,'profesiones':profesionesBdd})

def procesarActualizacionProfesion(request):
    id_ac=request.POST["id_ac"]
    nombre_ac=request.POST["nombre_ac"]
    años_experiencia_ac=request.POST["años_experiencia_ac"]
    descripcion_ac=request.POST["descripcion_ac"]
    titulos_ac=request.POST["titulos_ac"]


    #Insertando datos mediante el ORM de DJANGO
    profesionEditar=Profesion.objects.get(id_ac=id_ac)
    profesionEditar.nombre_ac=nombre_ac
    profesionEditar.años_experiencia_ac=años_experiencia_ac
    profesionEditar.titulos_ac=titulos_ac

    profesionEditar.descripcion_ac=descripcion_ac
    profesionEditar.save()
    messages.success(request,
      'PROFESION ACTUALIZADA EXITOSAMENTE')
    return redirect('/listadoProfesiones/')



def listadoAutores(request):
    autoresBdd = Autor.objects.all()
    profesionBdd = Profesion.objects.all()

    return render(request, 'listadoAutores.html', {'autores': autoresBdd, 'profesiones': profesionBdd})

def eliminarAutor(request,id_ac):
    autorEliminar=Autor.objects.get(id_ac=id_ac)
    autorEliminar.delete()
    messages.success(request, 'AUTOR ELIMINADO EXITOSAMENTE')
    return redirect('/listadoAutores/')

def guardarAutor(request):
    id_profesion=request.POST["id_profesion"]
    #capturando el tipo seleccionado por el usuario
    profesionSeleccionado=Profesion.objects.get(id_ac=id_profesion)

    nombre_ac=request.POST["nombre_ac"]
    correo_electronico_ac=request.POST["correo_electronico_ac"]
    observaciones_ac=request.POST["observaciones_ac"]

    hoja_vida_ac=request.FILES.get("hoja_vida_ac")

    #Insertando datos mediante el ORM de django

    autor = Autor.objects.create(
        nombre_ac=nombre_ac,
        correo_electronico_ac=correo_electronico_ac,
        observaciones_ac=observaciones_ac,
        profesion_ac=profesionSeleccionado,
        hoja_vida_ac=hoja_vida_ac,

    )

    messages.success(request, 'AUTOR GUARDADO EXITOSAMENTE')
    return redirect('/listadoAutores/')


def editarAutor(request, id_bg):
    autorEditar=Autor.objects.get(id_bg=id_bg)
    profesionesBdd = Profesion.objects.all()
    return  render(request, 'editarAutor.html',{'autor':autorEditar, 'profesiones': profesionesBdd})


def procesarActualizacionAutor(request):
    # Obtener los datos del formulario
    id_ac = request.POST["id_ac"]
    id_profesion = request.POST["id_profesion"]
    profesionSeleccionado = Profesion.objects.get(id_ac=id_profesion)
    nombre_ac = request.POST["nombre_ac"]
    correo_electronico_ac = request.POST["correo_electronico_ac"]
    observaciones_ac = request.POST["observaciones_ac"]
    hoja_vida_ac = request.FILES.get('hoja_vida_ac')


    # Verificar si 'fotografia' está presente en request.FILES
    if hoja_vida_ac is not None:
        hoja_vida_ac.fotografia_referencia = hoja_vida_ac


        # Guardar el archivo en el sistema de archivos


    # Actualizar el propietario con los nuevos datos
    autorEditar = Autor.objects.get(id_ac=id_ac)
    autorEditar.profesion = profesionSeleccionado
    autorEditar.nombre_ac = nombre_ac
    autorEditar.correo_electronico_ac = correo_electronico_ac
    autorEditar.observaciones_ac = observaciones_ac

    autorEditar.save()

    messages.success(request, 'AUTOR ACTUALIZADO EXITOSAMENTE')
    return redirect('/listadoAutores/')

def listadoLibros(request):
    librosBdd = Libro.objects.all()
    generoBdd = GeneroLiterario.objects.all()
    autorBdd = Autor.objects.all()
    return render(request, 'listadoLibros.html', {'libros': librosBdd, 'generos': generoBdd, 'autores': autorBdd})

def eliminarLibro(request, id_ac):
    try:
        libroEliminar = Libro.objects.get(id_ac=id_ac)
        libroEliminar.delete()
        messages.success(request, 'LIBRO ELIMINADO EXITOSAMENTE')
    except Libro.DoesNotExist:
        messages.error(request, 'El libro no existe')

    return redirect('/listadoLibros/')

def guardarLibro(request):
    if request.method == 'POST':
        try:
            id_genero = request.POST["id_genero"]
            id_autor = request.POST["id_autor"]
            generoSeleccionado = GeneroLiterario.objects.get(id_ac=id_genero)
            autorSeleccionado = Autor.objects.get(id_ac=id_autor)
            titulo_ac = request.POST["titulo_ac"]
            editorial_ac = request.POST["editorial_bg"]
            descripcion_ac = request.POST["descripcion_ac"]
            fotografia_ac = request.FILES.get("fotografia_ac")

            # Insertando datos mediante el ORM de Django
            libro = Libro.objects.create(
                titulo_ac=titulo_ac,
                editorial_ac=editorial_ac,
                descripcion_ac=descripcion_ac,
                genero_ac=generoSeleccionado,
                autor_ac=autorSeleccionado,
                fotografia_ac=fotografia_ac,
            )

            messages.success(request, 'LIBRO GUARDADO EXITOSAMENTE')
        except (GeneroLiterario.DoesNotExist, Autor.DoesNotExist):
            messages.error(request, 'Género o autor no existen')

    return redirect('/listadoLibros/')

def editarLibro(request, id_ac):
    try:
        libroEditar = Libro.objects.get(id_ac=id_ac)
        generosBdd = GeneroLiterario.objects.all()
        autoresBdd = Autor.objects.all()
        return render(request, 'editarLibro.html', {'libro': libroEditar, 'generos': generosBdd, 'autores': autoresBdd})
    except Libro.DoesNotExist:
        messages.error(request, 'El libro no existe')
        return redirect('/listadoLibros/')

def procesarActualizacionLibro(request):
    if request.method == 'POST':
        id_ac = request.POST["id_ac"]
        id_genero = request.POST["id_genero"]
        id_autor = request.POST["id_autor"]
        fotografia_ac = request.FILES.get('fotografia_ac')

        try:
            libroEditar = Libro.objects.get(id_ac=id_ac)
            generoSeleccionado = GeneroLiterario.objects.get(id_ac=id_genero)
            autorSeleccionado = Autor.objects.get(id_ac=id_autor)

            if fotografia_ac is not None:
                libroEditar.fotografia_ac = fotografia_ac
                # Guardar el archivo en el sistema de archivos


            libroEditar.genero_ac = generoSeleccionado
            libroEditar.autor_ac = autorSeleccionado
            libroEditar.titulo_ac = request.POST["titulo_ac"]
            libroEditar.editorial_ac = request.POST["editorial_ac"]
            libroEditar.descripcion_ac = request.POST["descripcion_ac"]

            libroEditar.save()

            messages.success(request, 'LIBRO ACTUALIZADO EXITOSAMENTE')
        except (Libro.DoesNotExist, GeneroLiterario.DoesNotExist, Autor.DoesNotExist):
            messages.error(request, 'El libro, género o autor no existen')

    return redirect('/listadoLibros/')
