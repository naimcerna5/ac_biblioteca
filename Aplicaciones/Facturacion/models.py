from django.db import models

class GeneroLiterario(models.Model):
    id_ac = models.AutoField(primary_key=True)
    nombre_ac = models.CharField(max_length=50)
    descripcion_ac = models.CharField(max_length=150)
    fotografia_ac = models.FileField(upload_to='genero', null=True, blank=True)

    def __str__(self):
        fila = "{0}:{1} {2} {3} "
        return fila.format(self.id_ac, self.nombre_ac, self.descripcion_ac)

class Profesion(models.Model):
    id_ac = models.AutoField(primary_key=True)
    nombre_ac = models.CharField(max_length=50)
    descripcion_ac = models.CharField(max_length=150)
    anos_experiencia_ac = models.PositiveIntegerField()
    titulos_ac = models.CharField(max_length=150)
    def __str__(self):
        fila = "{0}:{1} {2} {3}"
        return fila.format(self.id_ac, self.nombre_ac, self.descripcion_ac, self.anos_experiencia_ac, self.titulos_ac)

class Autor(models.Model):
    id_ac = models.AutoField(primary_key=True)
    nombre_ac = models.CharField(max_length=50)
    correo_electronico_ac = models.EmailField(blank=True)
    observaciones_ac = models.TextField()
    hoja_vida_ac = models.FileField(upload_to='autor', null=True, blank=True)
    profesion_ac = models.ForeignKey(Profesion, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}:{1} {2} {3}"
        return fila.format(self.id_ac, self.nombre_ac, self.observaciones_ac, self.correo_electronico_ac)

class Libro(models.Model):
    id_ac = models.AutoField(primary_key=True)
    titulo_ac = models.CharField(max_length=50)
    editorial_ac = models.CharField(max_length=50)
    descripcion_ac = models.TextField(blank=True, null=True)
    fotografia_ac = models.FileField(upload_to='libro', null=True, blank=True)
    genero_ac = models.ForeignKey(GeneroLiterario, null=True, blank=True, on_delete=models.PROTECT)
    autor_ac = models.ForeignKey(Autor, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}:{1} {2} {3} "
        return fila.format(self.id_ac, self.editorial_ac, self.descripcion_ac, self.titulo_ac)
